package com.mynbc.pack.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;

import com.mynbc.pack.base.BaseActivity;
import com.mynbc.pack.databinding.ActivityMainBinding;
import com.mynbc.pack.registeruser.RegisterUserActivity;

public class HomeScreenActivity extends BaseActivity<ActivityMainBinding, HomeViewModel> {


    @NonNull
    @Override
    protected HomeViewModel createViewModel() {
        HomeViewModelFactory factory = new HomeViewModelFactory();
        return ViewModelProviders.of(this, factory).get(HomeViewModel.class);
    }

    @NonNull
    @Override
    protected ActivityMainBinding createViewBinding(LayoutInflater layoutInflater) {
        return ActivityMainBinding.inflate(layoutInflater);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListeners();
        observeViewModel();
    }

    private void setListeners() {

        binding.internetImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent starter = new Intent(HomeScreenActivity.this, RegisterUserActivity.class);
                startActivity(starter);
            }
        });

    }

    private void observeViewModel() {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
