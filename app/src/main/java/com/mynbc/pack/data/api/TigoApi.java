package com.mynbc.pack.data.api;

import com.mynbc.pack.data.api.model.request.VodacomReqData;
import com.mynbc.pack.data.api.model.response.UserDetailResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface TigoApi {
    @POST("api.php")
    Call<UserDetailResponse> getNidaResponse(@HeaderMap Map<String, String> headers, @Body VodacomReqData body);
}
