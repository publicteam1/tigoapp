package com.mynbc.pack.data.api.model.request;

public class VodacomReqData {

    private String fingercode;
    private String nin;
    private String fingerimage;

    public void setFingercode(String fingercode){
        this.fingercode = fingercode;
    }

    public String getFingercode(){
        return fingercode;
    }

    public void setNin(String nin){
        this.nin = nin;
    }

    public String getNin(){
        return nin;
    }

    public void setFingerimage(String fingerimage){
        this.fingerimage = fingerimage;
    }

    public String getFingerimage(){
        return fingerimage;
    }
}
