package com.mynbc.pack.data.api.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserDetailResponse implements Serializable {

	@SerializedName("status_message")
	private String statusMessage;

	@SerializedName("data")
	private Data data;

	@SerializedName("status")
	private int status;

	public void setStatusMessage(String statusMessage){
		this.statusMessage = statusMessage;
	}

	public String getStatusMessage(){
		return statusMessage;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}
}