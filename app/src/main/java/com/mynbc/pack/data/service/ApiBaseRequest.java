package com.mynbc.pack.data.service;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ApiBaseRequest {

    protected JSONObject requestBody = new JSONObject();

    public ApiBaseRequest() {
        //Empty base constructor
    }

    public RequestBody getRequestBody() {
        return RequestBody.create(MediaType.parse("text/plain"), requestBody.toString().replaceAll("\\\\",""));
    }
}
