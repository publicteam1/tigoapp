package com.mynbc.pack.data.service;

import com.mynbc.pack.data.api.TigoApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    private static final String URL = "https://selcom.touchless.id/";
    private static ApiService singleton;
    private final TigoApi tigoApi;

    private ApiService() {
        Retrofit mRetrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(URL).build();

        tigoApi = mRetrofit.create(TigoApi.class);
    }

    public static ApiService getInstance() {
        if (singleton == null) {
            synchronized (ApiService.class) {
                if (singleton == null) {
                    singleton = new ApiService();
                }
            }
        }
        return singleton;
    }

    public TigoApi getVodacomApi() {
        return tigoApi;
    }
}
