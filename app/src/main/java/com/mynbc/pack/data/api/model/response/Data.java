package com.mynbc.pack.data.api.model.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data implements Serializable {

	@SerializedName("reference")
	private String reference;

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<DataItem> data;

	@SerializedName("resultcode")
	private String resultcode;

	@SerializedName("message")
	private String message;

	public void setReference(String reference){
		this.reference = reference;
	}

	public String getReference(){
		return reference;
	}

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<DataItem> data){
		this.data = data;
	}

	public List<DataItem> getData(){
		return data;
	}

	public void setResultcode(String resultcode){
		this.resultcode = resultcode;
	}

	public String getResultcode(){
		return resultcode;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}