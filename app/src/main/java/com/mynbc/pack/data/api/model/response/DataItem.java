package com.mynbc.pack.data.api.model.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DataItem implements Serializable {

	@SerializedName("NATIONALITY")
	private String nATIONALITY;

	@SerializedName("SURNAME")
	private String sURNAME;

	@SerializedName("RESIDENTREGION")
	private String rESIDENTREGION;

	@SerializedName("SEX")
	private String sEX;

	@SerializedName("FIRSTNAME")
	private String fIRSTNAME;

	@SerializedName("RESIDENTVILLAGE")
	private String rESIDENTVILLAGE;

	@SerializedName("RESIDENTPOSTCODE")
	private String rESIDENTPOSTCODE;

	@SerializedName("PLACEOFBIRTH")
	private List<Object> pLACEOFBIRTH;

	@SerializedName("NIN")
	private String nIN;

	@SerializedName("RESIDENTWARD")
	private String rESIDENTWARD;

	@SerializedName("MIDDLENAME")
	private String mIDDLENAME;

	@SerializedName("DATEOFBIRTH")
	private String dATEOFBIRTH;

	@SerializedName("RESIDENTDISTRICT")
	private String rESIDENTDISTRICT;

	@SerializedName("PHOTO")
	private String pHOTO;

	@SerializedName("RESIDENTSTREET")
	private String rESIDENTSTREET;

	public void setNATIONALITY(String nATIONALITY){
		this.nATIONALITY = nATIONALITY;
	}

	public String getNATIONALITY(){
		return nATIONALITY;
	}

	public void setSURNAME(String sURNAME){
		this.sURNAME = sURNAME;
	}

	public String getSURNAME(){
		return sURNAME;
	}

	public void setRESIDENTREGION(String rESIDENTREGION){
		this.rESIDENTREGION = rESIDENTREGION;
	}

	public String getRESIDENTREGION(){
		return rESIDENTREGION;
	}

	public void setSEX(String sEX){
		this.sEX = sEX;
	}

	public String getSEX(){
		return sEX;
	}

	public void setFIRSTNAME(String fIRSTNAME){
		this.fIRSTNAME = fIRSTNAME;
	}

	public String getFIRSTNAME(){
		return fIRSTNAME;
	}

	public void setRESIDENTVILLAGE(String rESIDENTVILLAGE){
		this.rESIDENTVILLAGE = rESIDENTVILLAGE;
	}

	public String getRESIDENTVILLAGE(){
		return rESIDENTVILLAGE;
	}

	public void setRESIDENTPOSTCODE(String rESIDENTPOSTCODE){
		this.rESIDENTPOSTCODE = rESIDENTPOSTCODE;
	}

	public String getRESIDENTPOSTCODE(){
		return rESIDENTPOSTCODE;
	}

	public void setPLACEOFBIRTH(List<Object> pLACEOFBIRTH){
		this.pLACEOFBIRTH = pLACEOFBIRTH;
	}

	public List<Object> getPLACEOFBIRTH(){
		return pLACEOFBIRTH;
	}

	public void setNIN(String nIN){
		this.nIN = nIN;
	}

	public String getNIN(){
		return nIN;
	}

	public void setRESIDENTWARD(String rESIDENTWARD){
		this.rESIDENTWARD = rESIDENTWARD;
	}

	public String getRESIDENTWARD(){
		return rESIDENTWARD;
	}

	public void setMIDDLENAME(String mIDDLENAME){
		this.mIDDLENAME = mIDDLENAME;
	}

	public String getMIDDLENAME(){
		return mIDDLENAME;
	}

	public void setDATEOFBIRTH(String dATEOFBIRTH){
		this.dATEOFBIRTH = dATEOFBIRTH;
	}

	public String getDATEOFBIRTH(){
		return dATEOFBIRTH;
	}

	public void setRESIDENTDISTRICT(String rESIDENTDISTRICT){
		this.rESIDENTDISTRICT = rESIDENTDISTRICT;
	}

	public String getRESIDENTDISTRICT(){
		return rESIDENTDISTRICT;
	}

	public void setPHOTO(String pHOTO){
		this.pHOTO = pHOTO;
	}

	public String getPHOTO(){
		return pHOTO;
	}

	public void setRESIDENTSTREET(String rESIDENTSTREET){
		this.rESIDENTSTREET = rESIDENTSTREET;
	}

	public String getRESIDENTSTREET(){
		return rESIDENTSTREET;
	}
}