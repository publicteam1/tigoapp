package com.mynbc.pack.data.api;

import android.content.Context;
import android.util.Log;

import com.mynbc.pack.data.api.model.request.VodacomReqData;
import com.mynbc.pack.data.api.model.response.UserDetailResponse;
import com.mynbc.pack.data.service.ApiService;
import com.mynbc.pack.fingerprints.FingerPrintNavigator;
import com.mynbc.pack.utils.CommonUtils;
import com.mynbc.pack.utils.Logger;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TigoRepository {

    private static final String TAG = TigoRepository.class.getSimpleName();
    private static TigoRepository vodacomRepository;
    private static TigoApi vodacomApi;
    private static final Map<String, String> map = new HashMap<>();

    public synchronized static TigoRepository getInstance() {
        map.put("action", "MATCH_NIN");

        if (vodacomRepository == null) {
            vodacomRepository = new TigoRepository();
            vodacomApi = ApiService.getInstance().getVodacomApi();
        }
        return vodacomRepository;
    }

    public void getNidaResponseMock(Context context, final FingerPrintNavigator.LoadVodacomCallback callback) throws IOException {
        Gson gson = new Gson();
        UserDetailResponse userDetailResponse = gson.fromJson(CommonUtils.loadJSONFromAsset(context, "nida_mock.json"), UserDetailResponse.class);
        callback.onUserDetailLoaded(userDetailResponse);
    }

    public void getUserDetails(String nin, String fingerCode, String fingerImage, final FingerPrintNavigator.LoadVodacomCallback callback) throws IOException {
        VodacomReqData vodacomRequest = new VodacomReqData();
        vodacomRequest.setFingercode(fingerCode);
        vodacomRequest.setNin(nin);
        vodacomRequest.setFingerimage(fingerImage);

        Log.d(TAG, "NIDA API FingerCode: " +fingerCode);
        Logger.log(TAG, "FingerCode: " +fingerCode);
        Log.d(TAG, "NIDA API NIN: " + nin);
        Logger.log(TAG, "NIN: " +nin);

        vodacomApi.getNidaResponse(map, vodacomRequest).enqueue(new Callback<UserDetailResponse>() {
            @Override
            public void onResponse(Call<UserDetailResponse> call, Response<UserDetailResponse> response) {
                Log.d(TAG, "NIDA API response: " + response.code() + " : " + response.message());

                Gson gson = new Gson();
                Log.d(TAG, "NIDA API response body: " + gson.toJson(response.body()));
                Logger.log(TAG, "NIDA API response body: " + gson.toJson(response.body()));

                if (response.body() != null) {
                    callback.onUserDetailLoaded(response.body());
                } else {
                    callback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<UserDetailResponse> call, Throwable t) {
                Log.d(TAG, "NIDA API error: " + t.getMessage());
                Logger.log(TAG, "NIDA API error: " + t.getMessage());
                callback.onError();
            }
        });

    }
}
