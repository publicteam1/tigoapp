package com.mynbc.pack.fingerprints;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class FingerPrintViewModelFactory implements ViewModelProvider.Factory {

    public FingerPrintViewModelFactory() {

    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if (modelClass.isAssignableFrom(FingerPrintsViewModel.class)) {
            return (T) new FingerPrintsViewModel();
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
