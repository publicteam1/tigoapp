package com.mynbc.pack.fingerprints;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mynbc.pack.base.BaseViewModel;
import com.mynbc.pack.data.api.TigoRepository;
import com.mynbc.pack.data.api.model.response.UserDetailResponse;
import com.mynbc.pack.data.model.IdentySdkResponse;
import com.mynbc.pack.utils.LocalSession;

import java.io.IOException;
import java.util.List;

public class FingerPrintsViewModel extends BaseViewModel {

    private final MutableLiveData<UserDetailResponse> vodacomResponseLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> showErrorMessageLiveData = new MutableLiveData<>();
    private final MutableLiveData<Void> showLoadingLiveData = new MutableLiveData<>();
    private final MutableLiveData<Void> hideLoadingLiveData = new MutableLiveData<>();
    private static final String TAG = FingerPrintsViewModel.class.getSimpleName();

    private final UserDetailResponseCallback responseCallback = new UserDetailResponseCallback();

    public LiveData<UserDetailResponse> getUserDetails() {
        return vodacomResponseLiveData;
    }
    private boolean isSuccessResponse = false;
    private int requestCount = 0;
    private int responseCount = 0;

    public void callUserDetailApi(Context context, List<IdentySdkResponse> responseList) throws IOException {
        setIsLoading(true);
        String nin = LocalSession.getString(context, LocalSession.NIN, "");
        if(responseList !=null && responseList.size() > 0) {
            for (IdentySdkResponse identySdkResponse : responseList) {

                if (isSuccessResponse)
                    break;

                String fingerCode = identySdkResponse.getFingerCode();
                String wsqImg = identySdkResponse.getB64Wq();
                requestCount++;
                TigoRepository.getInstance().getUserDetails(nin, fingerCode, wsqImg, responseCallback);

            }
        }
    }


    private void setIsLoading(boolean loading) {
        if (loading) {
            showLoadingLiveData.postValue(null);
        } else {
            hideLoadingLiveData.postValue(null);
        }
    }


    private void setUserDetailsLiveData(UserDetailResponse userDetailResponse) {
        setIsLoading(false);
        responseCount++;
        if (userDetailResponse.getData().getResultcode().equalsIgnoreCase("000")) {
            isSuccessResponse = true;
            this.vodacomResponseLiveData.postValue(userDetailResponse);
        } else if(responseCount == requestCount){
                this.vodacomResponseLiveData.postValue(userDetailResponse);
        }

        Log.d(TAG, "NIDA API requestCount: " +requestCount);
        Log.d(TAG, "NIDA API responseCount: " +responseCount);

    }

    public LiveData<Void> getShowLoadingLiveData() {
        return showLoadingLiveData;
    }

    public LiveData<String> getShowErrorMessageLiveData() {
        return showErrorMessageLiveData;
    }

    public LiveData<Void> getHideLoadingLiveData() {
        return hideLoadingLiveData;
    }

    private class UserDetailResponseCallback implements FingerPrintNavigator.LoadVodacomCallback {

        @Override
        public void onUserDetailLoaded(UserDetailResponse userDetailResponse) {
            setUserDetailsLiveData(userDetailResponse);
        }

        @Override
        public void onDataNotAvailable() {
            setIsLoading(false);
            showErrorMessageLiveData.postValue("There is not items!");
        }

        @Override
        public void onError() {
            setIsLoading(false);
            showErrorMessageLiveData.postValue("Something Went Wrong!");
        }
    }


}
