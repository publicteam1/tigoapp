package com.mynbc.pack.fingerprints;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


import com.mynbc.pack.base.BaseActivity;
import com.mynbc.pack.data.api.model.response.UserDetailResponse;
import com.mynbc.pack.data.model.IdentySdkResponse;
import com.mynbc.pack.databinding.ActivityFingersPrintBinding;
import com.mynbc.pack.userdetails.UserDetailsActivity;
import com.mynbc.pack.utils.MissingFingerUtils;
import com.identy.Attempt;
import com.identy.FingerOutput;
import com.identy.IdentyError;
import com.identy.IdentyResponse;
import com.identy.IdentyResponseListener;
import com.identy.IdentySdk;
import com.identy.InitializationListener;
import com.identy.InlineGuideOption;
import com.identy.TemplateSize;
import com.identy.WSQCompression;
import com.identy.enums.Finger;
import com.identy.enums.FingerDetectionMode;
import com.identy.enums.Hand;
import com.identy.enums.Template;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class FingerPrintsActivity extends BaseActivity<ActivityFingersPrintBinding, FingerPrintsViewModel> {

    private static final String TAG = "MenuFinger";
    static String NET_KEY = "";
    static boolean showProgressDialog = true;
    boolean enableSpoofCheck = false;
    String mode = "demo";
    WSQCompression compression = WSQCompression.WSQ_10_1;
    int base64encoding = Base64.DEFAULT;
    boolean displayboxes = false;
    boolean displayBoxImage = false;
//    String licenseFile = "1447_com.app.identy.tigo2022-03-31 00_00_00.lic";
    String licenseFile="1756_com.mynbc.pack2022-12-12 00_00_00.lic";
    boolean isResponseReceived = false;
    private FingerDetectionMode[] detectionModes;
    private Context mContext;
    private boolean isDisclaimerAccepted = false;
    private HashMap<Template, HashMap<Finger, ArrayList<TemplateSize>>> requiredtemplates;
    private List<IdentySdkResponse> identySdkResponseList;
    IdentyResponseListener responseListener = new IdentyResponseListener() {


        @Override
        public void onAttempt(Hand hand, int attemptCount, Map<Finger, Attempt> attempt) {

        }

        @Override
        public void onResponse(IdentyResponse response, HashSet<String> transactionIds) {
            response.getPrints();
            identySdkResponseList = new ArrayList<>();

            for (Map.Entry<Pair<Hand, Finger>, FingerOutput> o : response.getPrints().entrySet()) {
                Pair<Hand, Finger> handFinger = o.getKey();
                FingerOutput fingerOutput = o.getValue();
                for (Template template : Template.values()) {

                    try {
                        if (fingerOutput.getTemplates().containsKey(template)) {
                            Log.d("NEW_TEMPLATE_SIZE", "onResponse: it has template: " + template.toString());

                            TemplateSize[] templateSizes = TemplateSize.values();
                            for (TemplateSize templateSize : templateSizes) {
                                if (fingerOutput.getTemplates().get(template).containsKey(templateSize)) {
                                    String base64Str = fingerOutput.getTemplates().get(template).get(templateSize);
                                    Log.d("NEW_TEMPLATE_SIZE", "onResponse: base64Str null ->" + (base64Str == null));
                                    String fileCode = getFileNamingConvention(handFinger.first, handFinger.second);
                                    String fingerCode = null;

                                    if (fileCode.equals("12")) {
                                        fingerCode = "L1";
                                    } else if (fileCode.equals("11")) {
                                        fingerCode = "R1";
                                    } else if (fileCode.equals("07")) {
                                        fingerCode = "L2";
                                    } else if (fileCode.equals("08")) {
                                        fingerCode = "L3";
                                    } else if (fileCode.equals("02")) {
                                        fingerCode = "R2";
                                    } else if (fileCode.equals("03")) {
                                        fingerCode = "R4";
                                    }
                                    IdentySdkResponse identySdkResponse = new IdentySdkResponse(base64Str, fingerCode);
                                    identySdkResponseList.add(identySdkResponse);
                                }
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                viewModel.callUserDetailApi(mContext, identySdkResponseList);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onErrorResponse(IdentyError error, HashSet<String> transactionIds) {

            Toast.makeText(FingerPrintsActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();

        }

    };

    @NonNull
    @Override
    protected FingerPrintsViewModel createViewModel() {
        FingerPrintViewModelFactory factory = new FingerPrintViewModelFactory();
        return ViewModelProviders.of(this, factory).get(FingerPrintsViewModel.class);
    }

    @NonNull
    @Override
    protected ActivityFingersPrintBinding createViewBinding(LayoutInflater layoutInflater) {
        return ActivityFingersPrintBinding.inflate(layoutInflater);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_LOGS}, 1);
            }
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_LOGS}, 2);
            }
            if (checkSelfPermission(Manifest.permission.READ_LOGS) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_LOGS}, 3);
            }

        }

        displayboxes = true;
        displayBoxImage = false;
        compression = WSQCompression.WSQ_10_1;
        setListeners();
        observeViewModel();
    }

    private void setListeners() {
        binding.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isDisclaimerAccepted = isChecked;

            }
        });

        binding.nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isDisclaimerAccepted) {
                    Toast.makeText(mContext, "Please accept disclaimer notice.", Toast.LENGTH_LONG).show();
                } else {
                    iniFingerCapture();
                }
            }
        });

        binding.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void observeViewModel() {

        viewModel.getUserDetails().observe(this, new Observer<UserDetailResponse>() {
            @Override
            public void onChanged(UserDetailResponse userDetailResponse) {

                if (!isResponseReceived) {
                    isResponseReceived = true;

                    if (userDetailResponse.getData().getResultcode().equalsIgnoreCase("000")) {
                        Toast.makeText(FingerPrintsActivity.this, "Verified successfully", Toast.LENGTH_SHORT).show();
                        Intent starter = new Intent(FingerPrintsActivity.this, UserDetailsActivity.class);
                        starter.putExtra("UserDetailResponse", userDetailResponse);
                        startActivity(starter);
                    } else {

                        new AlertDialog.Builder(mContext)
                                .setMessage(userDetailResponse.getData().getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })

                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                }

            }
        });

        viewModel.getShowLoadingLiveData().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                binding.progressBar.setVisibility(View.VISIBLE);
            }
        });

        viewModel.getHideLoadingLiveData().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                binding.progressBar.setVisibility(View.GONE);
            }
        });

        viewModel.getShowErrorMessageLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String message) {
                new AlertDialog.Builder(mContext)
                        .setMessage(message)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })

                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    private void iniFingerCapture() {
        updateIntent();
        try {
            fillRequiredTemplates();
            Log.d("TAG","Initializing sdk");
            IdentySdk.newInstance(FingerPrintsActivity.this, licenseFile, new InitializationListener<IdentySdk>() {
                @Override
                public void onInit(IdentySdk d) {
                    try {
                        Log.d("TAG","Setup of Camera");
                        d.setAllowTabletLandscape(true);
                        d.setBase64EncodingFlag(base64encoding);
                        d.disableMoveNextDetectionDialog();
                        d.disableTraining();
                        d.displayResult(false);
                        d = d.setDisplayImages(false).setMode(mode).setInlineGuide(false, new InlineGuideOption(300, 3)).setAS(enableSpoofCheck).setCalculateNFIQ(false)
                                .setDetectionMode(detectionModes)
                                .displayImages(mode.equals("demo")).setDisplayBoxes(displayboxes).setWSQCompression(compression);
                        d.setDebug(true);
                        d.setRequiredTemplates(requiredtemplates);

                        d.capture();

                    } catch (Exception e) {
                        Log.d("TAG","Failed to Initializing sdk");
                        e.printStackTrace();
                    }


                }
            }, responseListener, NET_KEY, showProgressDialog);


        } catch (Exception e) {
            Log.d("TAG","Failed to Initializing sdk");
            e.printStackTrace();
        }
    }

    private void updateIntent() {
        ArrayList<FingerDetectionMode> modes = new ArrayList<>();
        List<FingerDetectionMode> fingers = MissingFingerUtils.getLeftMissingFingers(FingerPrintsActivity.this);
        if (fingers.isEmpty() || fingers.size() == 4) {
            modes.add(FingerDetectionMode.L4F);
            modes.add(FingerDetectionMode.R4F);

        } else {
            modes.addAll(fingers);
        }
        detectionModes = modes.toArray(new FingerDetectionMode[modes.size()]);
    }

    private void fillRequiredTemplates() {
        requiredtemplates = new HashMap<>();
        ArrayList<TemplateSize> wsqTemplateSizes = new ArrayList<>();
        wsqTemplateSizes.add(TemplateSize.DEFAULT);
        HashMap<Finger, ArrayList<TemplateSize>> wsqFingerToGetTemplatesFor = new HashMap<>();
        wsqFingerToGetTemplatesFor.put(Finger.INDEX, wsqTemplateSizes);
        wsqFingerToGetTemplatesFor.put(Finger.MIDDLE, wsqTemplateSizes);
        wsqFingerToGetTemplatesFor.put(Finger.RING, wsqTemplateSizes);
        wsqFingerToGetTemplatesFor.put(Finger.LITTLE, wsqTemplateSizes);
        requiredtemplates.put(Template.WSQ, wsqFingerToGetTemplatesFor);
    }

    String getFileNamingConvention(Hand hand, Finger finger) {

        if (hand.equals(Hand.RIGHT)) {
            if (finger.equals(Finger.INDEX)) {
                return "02";
            } else if (finger.equals(Finger.MIDDLE)) {
                return "03";
            } else if (finger.equals(Finger.RING)) {
                return "04";
            } else if (finger.equals(Finger.LITTLE)) {
                return "05";
            } else if (finger.equals(Finger.THUMB)) {
                return "11";
            }


        } else if (hand.equals(Hand.LEFT)) {
            if (finger.equals(Finger.INDEX)) {
                return "07";
            } else if (finger.equals(Finger.MIDDLE)) {
                return "08";
            } else if (finger.equals(Finger.RING)) {
                return "09";
            } else if (finger.equals(Finger.LITTLE)) {
                return "10";
            } else if (finger.equals(Finger.THUMB)) {
                return "12";
            }
        }
        return "";
    }


    /*
     *   saving the files
     */

    String getUserDirectoryName() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        return sharedPreferences.getString("identy_user", "anonymous") + "_" + sharedPreferences.getString("identy_user_created_ts", "anonymous");
    }

    int getUserEnrollCount() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        return sharedPreferences.getInt("enroll_c", -1);
    }

    int getUserVerifyCount() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        return sharedPreferences.getInt("verify_c", -1);
    }

    int incrementUserEnrollCount() {
        int c = getUserEnrollCount();
        c += 1;
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        pref.edit().putInt("enroll_c", c).apply();
        return c;
    }

    int incrementUserVerifyCount() {
        int c = getUserVerifyCount();
        c += 1;
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        pref.edit().putInt("verify_c", c).apply();
        return c;
    }

}
