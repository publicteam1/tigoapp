package com.mynbc.pack.fingerprints;

import com.mynbc.pack.data.api.model.response.UserDetailResponse;

public interface FingerPrintNavigator {

    void getUserDetails(LoadVodacomCallback callback);

    void saveUserDetails(UserDetailResponse userDetailResponse);

    interface LoadVodacomCallback {
        void onUserDetailLoaded(UserDetailResponse userDetailResponse);

        void onDataNotAvailable();

        void onError();
    }
}
