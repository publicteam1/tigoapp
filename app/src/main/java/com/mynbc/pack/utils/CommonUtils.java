package com.mynbc.pack.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Base64;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class CommonUtils {

    private static final String TAG = CommonUtils.class.getSimpleName();

    public static String logLevel = "INFO";

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, StandardCharsets.UTF_8);
    }

    public static byte[] decode(String sourceStr, int base64encodeType) throws Exception {

        byte[] decodedBytes = Base64.decode(sourceStr.getBytes(), base64encodeType);
        return decodedBytes;
    }

    public static File createExternalDirectory(String folder) {


        String main_folder = Environment.getExternalStorageDirectory() + "/IDENTY";
        File main_dir = new File(main_folder);
        if (!main_dir.exists()) {
            main_dir.mkdir();
        }

        String dir_path = main_dir.toString() + "/" + folder;
        File dir = new File(dir_path);
        if (!dir.exists()) {
            dir.mkdir();
        }
        return dir;

    }

}
