package com.mynbc.pack.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.mynbc.pack.BuildConfig;

public class LocalSession {

    private static final String app_name = BuildConfig.APPLICATION_ID;
    public static String NIN = "nin";

    public static void setString(Context c, String key, String value) {
        if (c != null) {
            SharedPreferences.Editor editor = c.getSharedPreferences(app_name,
                    Context.MODE_PRIVATE).edit();
            editor.putString(key, value);
            editor.apply();
            SharedPreferences prefs = c.getSharedPreferences(app_name,
                    Context.MODE_PRIVATE);
            Log.d("setPrefString: ", prefs.getString(key, value) + "" + value);

        }
    }


    public static String getString(Context c, String key, String default_value) {
        if (c == null) {
            return default_value;
        } else {
            SharedPreferences prefs = c.getSharedPreferences(app_name,
                    Context.MODE_PRIVATE);
            Log.d("setString: ", prefs.getString(key, default_value) + "" + default_value);
            return prefs.getString(key, default_value);
        }
    }
}
