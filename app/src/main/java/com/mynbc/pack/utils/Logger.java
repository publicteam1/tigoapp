package com.mynbc.pack.utils;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Logger {

    private static final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/IDENTY";// Folder path


    public static  void log(String TAG,String body){
        log(TAG,body, true,"IdentyLog");
    }



    public static  void log(String TAG,String body, Boolean toFile,String filename){

        FileWriter fw = null;
        if(toFile){
            try{
                //delete old logs
                File folder = new File(path);
                if (folder.exists()) {
                    File[] listFiles = folder.listFiles();
                    long eligibleForDeletion = System.currentTimeMillis() - (14 * 24 * 60 * 60 * 1000L);
                    for (File listFile : listFiles) {
                        if (listFile.lastModified() < eligibleForDeletion) {
                            listFile.delete();
                        }
                    }
                }
                File folderFile = new File(path);
                if (!folderFile.exists()) {
                    folderFile.mkdirs();
                }
                File myFile = new File(folderFile, filename+formatDate(System.currentTimeMillis())+".txt");
                if(!myFile.exists())
                    myFile.createNewFile();

                fw = new FileWriter(myFile, true);
                formatTime(System.currentTimeMillis());
                fw.write(formatTime(System.currentTimeMillis())+ " - " +TAG+" - "+ body +"\n");
                fw.close();
            }catch(Exception e){
                log(TAG,"ERROR LOG TO FILE:"+body, false,"");
            }finally{
                if(fw != null){
                    try{
                        fw.close();
                    }catch(Exception e){
                        Logger.log("Exception occurred while closing the filewriter", e.getMessage());
                    }
                }
            }
        }

        Log.d(TAG,body);

    }


    public static String formatDate(Long time){
        if (time != null) {
            SimpleDateFormat resultSdf = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());


            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            String result = resultSdf.format(calendar.getTime());


            try {
                return result;
            } catch (Exception e) {
                log("formatTime", e.getMessage());
                return "2017-01-01";
            }
        } else
            return "2017-01-01";

    }

    public static String formatTime(Long time){
        if (time != null) {
            SimpleDateFormat resultSdf = new SimpleDateFormat("yyyy-MM-dd', 'HH:mm:ss",Locale.getDefault());


            // Create a calendar object that will convert the date and time value in milliseconds to date.
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            String result = resultSdf.format(calendar.getTime());


            try {
                return result;
            } catch (Exception e) {
                log("formatTime", e.getMessage());
                return "2016-01-01T01:01:01.001+0000";
            }
        } else
            return "2016-01-01T01:01:01.001+0000";
    }


}
