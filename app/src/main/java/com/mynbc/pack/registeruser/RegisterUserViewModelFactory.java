package com.mynbc.pack.registeruser;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class RegisterUserViewModelFactory implements ViewModelProvider.Factory {

    public RegisterUserViewModelFactory() {

    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if (modelClass.isAssignableFrom(RegisterUserViewModel.class)) {
            return (T) new RegisterUserViewModel();
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
