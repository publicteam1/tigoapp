package com.mynbc.pack.registeruser;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;

import com.mynbc.pack.base.BaseActivity;
import com.mynbc.pack.databinding.ActivityRegisterUserBinding;
import com.mynbc.pack.fingerprints.FingerPrintsActivity;
import com.mynbc.pack.utils.LocalSession;

public class RegisterUserActivity extends BaseActivity<ActivityRegisterUserBinding, RegisterUserViewModel> {


    @NonNull
    @Override
    protected RegisterUserViewModel createViewModel() {
        RegisterUserViewModelFactory factory = new RegisterUserViewModelFactory();
        return ViewModelProviders.of(this, factory).get(RegisterUserViewModel.class);
    }

    @NonNull
    @Override
    protected ActivityRegisterUserBinding createViewBinding(LayoutInflater layoutInflater) {
        return ActivityRegisterUserBinding.inflate(layoutInflater);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListeners();
        observeViewModel();
    }

    private void setListeners() {

        binding.nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateIdNo(binding.idNoEdt.getText().toString())) {
                    LocalSession.setString(RegisterUserActivity.this, LocalSession.NIN, binding.idNoEdt.getText().toString());
                    Intent starter = new Intent(RegisterUserActivity.this, FingerPrintsActivity.class);
                    startActivity(starter);
                }
            }
        });

        binding.addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateIdNo(binding.idNoEdt.getText().toString())) {
                  binding.packageLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        binding.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private boolean validateIdNo(String idNo) {
        String idNumber = idNo.trim();
        if(idNumber.equalsIgnoreCase("")){
            binding.idNoEdt.setError("ID number should not be empty");
            return false;
        } else if(idNumber.length() != 20) {
            binding.idNoEdt.setError("Please enter 20 digit NIN number");
            return false;
        }
        binding.idNoEdt.setError(null);
        return true;
    }

    private void observeViewModel() {
    }
}
