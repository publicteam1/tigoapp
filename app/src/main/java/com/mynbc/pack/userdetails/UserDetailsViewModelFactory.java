package com.mynbc.pack.userdetails;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class UserDetailsViewModelFactory implements ViewModelProvider.Factory {

    public UserDetailsViewModelFactory() {

    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if (modelClass.isAssignableFrom(UserDetailsViewModel.class)) {
            return (T) new UserDetailsViewModel();
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
