package com.mynbc.pack.userdetails;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;

import com.mynbc.pack.base.BaseActivity;
import com.mynbc.pack.data.api.model.response.DataItem;
import com.mynbc.pack.data.api.model.response.UserDetailResponse;
import com.mynbc.pack.databinding.ActivityUserDetailsBinding;
import com.mynbc.pack.home.HomeScreenActivity;

import java.util.List;

public class UserDetailsActivity extends BaseActivity<ActivityUserDetailsBinding, UserDetailsViewModel> {


    @NonNull
    @Override
    protected UserDetailsViewModel createViewModel() {
        UserDetailsViewModelFactory factory = new UserDetailsViewModelFactory();
        return ViewModelProviders.of(this, factory).get(UserDetailsViewModel.class);
    }

    @NonNull
    @Override
    protected ActivityUserDetailsBinding createViewBinding(LayoutInflater layoutInflater) {
        return ActivityUserDetailsBinding.inflate(layoutInflater);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListeners();
        observeViewModel();
        UserDetailResponse userDetailResponse = (UserDetailResponse) getIntent().getSerializableExtra("UserDetailResponse");
        List<DataItem> dataItemList = userDetailResponse.getData().getData();
        if(dataItemList != null && dataItemList.size() > 0){
            DataItem dataItem = dataItemList.get(0);
            setupUI(dataItem);
        }
    }

    public void setupUI(DataItem dataItem) {

        String name = dataItem.getFIRSTNAME()+" "+dataItem.getMIDDLENAME()+" "+dataItem.getSURNAME();
        binding.nameTxt.setText(name);
        if(dataItem.getNIN() != null && !dataItem.getNIN().equalsIgnoreCase("")) {
            String firstFour = "";
            String lastFour = "";
            if(dataItem.getNIN().length() == 20) {
                firstFour = dataItem.getNIN().substring(0, 4);
                lastFour = dataItem.getNIN().substring(16, 20);
            } else if(dataItem.getNIN().length() > 4){
                firstFour = dataItem.getNIN().substring(0, 4);
            }
            binding.ninTxt.setText(firstFour + "xxxxx" + lastFour);
        }
        binding.sexTxt.setText(dataItem.getSEX());
        binding.dobTxt.setText(dataItem.getDATEOFBIRTH());
        byte[] decodedString = Base64.decode(dataItem.getPHOTO(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        binding.userImg.setImageBitmap(decodedByte);

    }

    private void setListeners() {

        binding.completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserDetailsActivity.this, HomeScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        binding.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void observeViewModel() {
    }
}
